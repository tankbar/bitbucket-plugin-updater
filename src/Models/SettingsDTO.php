<?php

namespace TBPU\Models;

use TBPU\Exceptions\MissingSettingException;

class SettingsDTO {
	private string $current_version = '';
	private string $plugin_name = '';
	private string $plugin_description = '';
	private string $plugin_installation = '';
	private string $plugin_changelog = '';
	private string $plugin_slug = '';
	private string $plugin_author = '';
	private string $plugin_homepage = '';
	private string $plugin_requires_php = '';
	private string $plugin_requires_wp = '';
	private string $plugin_tested_with_wp = '';
	private string $bitbucket_workspace = '';
	private string $bitbucket_repository_slug = '';
	private string $bitbucket_repository_username = '';
	private string $bitbucket_repository_password = '';

	/**
	 * @return string
	 */
	public function get_current_version(): string {
		return $this->current_version;
	}

	/**
	 * @param string $current_version
	 *
	 * @return SettingsDTO
	 */
	public function set_current_version( string $current_version ): SettingsDTO {
		$this->current_version = $current_version;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_name(): string {
		return $this->plugin_name;
	}

	/**
	 * @param string $plugin_name
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_name( string $plugin_name ): SettingsDTO {
		$this->plugin_name = $plugin_name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_description(): string {
		return $this->plugin_description;
	}

	/**
	 * @param string $plugin_description
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_description( string $plugin_description ): SettingsDTO {
		$this->plugin_description = $plugin_description;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_installation(): string {
		return $this->plugin_installation;
	}

	/**
	 * @param string $plugin_installation
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_installation( string $plugin_installation ): SettingsDTO {
		$this->plugin_installation = $plugin_installation;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_changelog(): string {
		return $this->plugin_changelog;
	}

	/**
	 * @param string $plugin_changelog
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_changelog( string $plugin_changelog ): SettingsDTO {
		$this->plugin_changelog = $plugin_changelog;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_slug(): string {
		return $this->plugin_slug;
	}

	/**
	 * @param string $plugin_slug
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_slug( string $plugin_slug ): SettingsDTO {
		$this->plugin_slug = $plugin_slug;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_author(): string {
		return $this->plugin_author;
	}

	/**
	 * @param string $plugin_author
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_author( string $plugin_author ): SettingsDTO {
		$this->plugin_author = $plugin_author;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_homepage(): string {
		return $this->plugin_homepage;
	}

	/**
	 * @param string $plugin_homepage
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_homepage( string $plugin_homepage ): SettingsDTO {
		$this->plugin_homepage = $plugin_homepage;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_requires_php(): string {
		return $this->plugin_requires_php;
	}

	/**
	 * @param string $plugin_requires_php
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_requires_php( string $plugin_requires_php ): SettingsDTO {
		$this->plugin_requires_php = $plugin_requires_php;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_requires_wp(): string {
		return $this->plugin_requires_wp;
	}

	/**
	 * @param string $plugin_requires_wp
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_requires_wp( string $plugin_requires_wp ): SettingsDTO {
		$this->plugin_requires_wp = $plugin_requires_wp;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_plugin_tested_with_wp(): string {
		return $this->plugin_tested_with_wp;
	}

	/**
	 * @param string $plugin_tested_with_wp
	 *
	 * @return SettingsDTO
	 */
	public function set_plugin_tested_with_wp( string $plugin_tested_with_wp ): SettingsDTO {
		$this->plugin_tested_with_wp = $plugin_tested_with_wp;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_bitbucket_workspace(): string {
		return $this->bitbucket_workspace;
	}

	/**
	 * @param string $bitbucket_workspace
	 *
	 * @return SettingsDTO
	 */
	public function set_bitbucket_workspace( string $bitbucket_workspace ): SettingsDTO {
		$this->bitbucket_workspace = $bitbucket_workspace;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_bitbucket_repository_slug(): string {
		return $this->bitbucket_repository_slug;
	}

	/**
	 * @param string $bitbucket_repository_slug
	 *
	 * @return SettingsDTO
	 */
	public function set_bitbucket_repository_slug( string $bitbucket_repository_slug ): SettingsDTO {
		$this->bitbucket_repository_slug = $bitbucket_repository_slug;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_bitbucket_repository_username(): string {
		return $this->bitbucket_repository_username;
	}

	/**
	 * @param string $bitbucket_repository_username
	 *
	 * @return SettingsDTO
	 */
	public function set_bitbucket_repository_username( string $bitbucket_repository_username ): SettingsDTO {
		$this->bitbucket_repository_username = $bitbucket_repository_username;

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_bitbucket_repository_password(): string {
		return $this->bitbucket_repository_password;
	}

	/**
	 * @param string $bitbucket_repository_password
	 *
	 * @return SettingsDTO
	 */
	public function set_bitbucket_repository_password( string $bitbucket_repository_password ): SettingsDTO {
		$this->bitbucket_repository_password = $bitbucket_repository_password;

		return $this;
	}

	public function validate() {
		$fields = [
			'version'             => $this->get_current_version(),
			'name'                => $this->get_plugin_name(),
			'slug'                => $this->get_plugin_slug(),
			'author'              => $this->get_plugin_author(),
			'homepage'            => $this->get_plugin_homepage(),
			'requires_php'        => $this->get_plugin_requires_php(),
			'requires_wp'         => $this->get_plugin_requires_wp(),
			'tested_with_wp'      => $this->get_plugin_tested_with_wp(),
			'bitbucket_workspace' => $this->get_bitbucket_workspace(),
			'repository_slug'     => $this->get_bitbucket_repository_slug(),
			'repository_username' => $this->get_bitbucket_repository_username(),
			'repository_password' => $this->get_bitbucket_repository_password(),
		];

		foreach ( $fields as $key => $value ) {
			if ( empty( $value ) ) {
				throw new MissingSettingException( "Missing '$key'." );
			}
		}
	}
}
