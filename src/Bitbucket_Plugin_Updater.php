<?php

namespace TBPU;

use TBPU\Models\SettingsDTO;

class Bitbucket_Plugin_Updater {
	private SettingsDTO $settings;

	public function __construct( SettingsDTO $settings ) {
		$this->settings = $settings;

		$settings->validate();

		add_filter( 'plugins_api', [ $this, 'get_plugin_information' ], 10, 3 );
		add_filter( 'site_transient_update_plugins', [ $this, 'check_for_plugin_updates' ] );
		add_filter( 'upgrader_pre_download', [ $this, 'download_plugin_update' ], 10, 4 );
	}

	public function get_plugin_information( $data, $action, $args ) {
		if ( $args->slug !== $this->settings->get_plugin_slug() ) {
			return $data;
		}

		switch ( $action ) {
			case 'plugin_information':
				$data = (object) [
					'name'           => $this->settings->get_plugin_name(),
					'slug'           => $this->settings->get_plugin_slug(),
					'author'         => $this->settings->get_plugin_author(),
					'author_profile' => $this->settings->get_plugin_homepage(),
					'version'        => $this->settings->get_current_version(),
					'requires_php'   => $this->settings->get_plugin_requires_php(),
					'requires'       => $this->settings->get_plugin_requires_wp(),
					'tested'         => $this->settings->get_plugin_tested_with_wp(),
					'sections'       => [
						'description'  => $this->settings->get_plugin_description(),
						'installation' => $this->settings->get_plugin_installation(),
						'changelog'    => $this->settings->get_plugin_changelog(),
					],
				];

				break;
		}

		return $data;
	}

	public function check_for_plugin_updates( $transient ) {
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		$plugin_slug               = $this->settings->get_plugin_slug();
		$bitbucket_username        = $this->settings->get_bitbucket_repository_username();
		$bitbucket_password        = $this->settings->get_bitbucket_repository_password();
		$bitbucket_repository_slug = $this->settings->get_bitbucket_repository_slug();
		$bitbucket_workspace       = $this->settings->get_bitbucket_workspace();

		$data = get_transient( "{$plugin_slug}_version" );
		if ( false === $data ) {
			$encoded_password = base64_encode( "{$bitbucket_username}:{$bitbucket_password}" );
			$result           = wp_remote_get(
				"https://api.bitbucket.org/2.0/repositories/{$bitbucket_workspace}/{$bitbucket_repository_slug}/refs/tags?sort=-target.date",
				[
					'headers' => [
						'Accept'          => 'application/json',
						'Accept-Encoding' => 'gzip, deflate',
						'Authorization'   => "Basic $encoded_password",
						'Cache-Control'   => 'no-cache',
						'Connection'      => 'keep-alive',
						'Host'            => 'api.bitbucket.org',
					],
				]
			);

			if (
				! is_wp_error( $result ) &&
				! wp_remote_retrieve_response_code( $result ) != 200 &&
				! empty( wp_remote_retrieve_body( $result ) )
			) {
				$data = json_decode( $result['body'] );

				set_transient( "{$plugin_slug}_version", $result['body'], MINUTE_IN_SECONDS );
			}
		} else {
			$data = json_decode( $data );
		}

		if ( $data ) {
			$current_tag = current( $data->values );
			if ( $current_tag && version_compare( $this->settings->get_current_version(), $current_tag->name, '<' ) ) {
				$plugin_file_name = $this->get_plugin_file_name();

				$transient->response[ $plugin_file_name ] = (object) [
					'id'             => $plugin_slug,
					'slug'           => $plugin_slug,
					'plugin'         => $plugin_file_name,
					'new_version'    => $current_tag->name,
					'url'            => "https://bitbucket.org/{$bitbucket_workspace}/{$bitbucket_repository_slug}/commits/tag/{$current_tag->name}",
					'package'        => "https://bitbucket.org/{$bitbucket_workspace}/{$bitbucket_repository_slug}/get/{$current_tag->name}.zip",
					'requires'       => $this->settings->get_plugin_requires_wp(),
					'tested'         => $this->settings->get_plugin_tested_with_wp(),
					'requires_php'   => $this->settings->get_plugin_tested_with_wp(),
					'upgrade_notice' => "<p>{$current_tag->message}</p>",
				];
			}
		}

		return $transient;
	}

	public function download_plugin_update( $file, $package, $upgrader, $extra_data ) {
		$plugin_file_name = $this->get_plugin_file_name();
		if ( $plugin_file_name !== $extra_data['plugin'] ) {
			return $file;
		}

		$plugin_slug        = $this->settings->get_plugin_slug();
		$bitbucket_username = $this->settings->get_bitbucket_repository_username();
		$bitbucket_password = $this->settings->get_bitbucket_repository_password();

		$upgrader->skin->feedback( 'downloading_package', $package );

		$url_path     = parse_url( $package, PHP_URL_PATH );
		$url_filename = basename( $url_path );

		$temporary_file_path = wp_tempnam( $url_filename );

		$fp = fopen( $temporary_file_path, 'w+' );

		$encoded_password = base64_encode( "{$bitbucket_username}:{$bitbucket_password}" );
		$request_headers  = [
			"Authorization: Basic $encoded_password",
		];

		$ch = curl_init( $package );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 600 );
		curl_setopt( $ch, CURLOPT_FILE, $fp );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $request_headers );
		curl_exec( $ch );
		curl_close( $ch );

		if ( file_exists( $temporary_file_path ) ) {
			$zip = new \ZipArchive();
			if ( $zip->open( $temporary_file_path ) ) {
				$bad_dir_name   = $zip->getNameIndex( 0 );
				$zip_name_index = 0;

				$zip_rename_operation_result = true;
				while ( $item_name = $zip->getNameIndex( $zip_name_index ) ) {
					if (
						! $zip->renameName(
							$item_name,
							str_replace( $bad_dir_name, "{$plugin_slug}/", $item_name )
						)
					) {
						$zip_rename_operation_result = false;
						break;
					}

					$zip_name_index ++;
				}

				if ( $zip_rename_operation_result ) {
					$file = $temporary_file_path;
				}

				$zip->close();
			}
		}

		return $file;
	}

	private function get_plugin_file_name() {
		$plugin_slug = $this->settings->get_plugin_slug();

		return "{$plugin_slug}/{$plugin_slug}.php";
	}
}
